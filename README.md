# YouTube Drummachine
  time to make some awesome beatskies.  
  
## Description
- Prototype beats using samples from YouTube
- Think of it as a YouTube Drummachine
  
## Inspiration
https://www.youtube.com/watch?v=5kZQfM2z9mk

## Notes
- Videos default to 360p. You can change that manually, after they're loaded.`
- Assuming that CORS isn't enable
by default, so no fear from access outside domain
- The client still has the option of making request
with $.get from the console, but is there any way 
to avoid this?
- Adding a new track with an empty track (I know, I know...) generates random volume and start
- Can also turn on random height and width in settings
- http://www.googledrive.com/host/0ByLhWX_ud385MVlMNXBjT2RWdUE

## Performance tips
- Disable all extensions (try going into Private/Anonymous/Incognito mode)

## Ideas
- make pattern buttons as little screenshots of the frame 
of the beginning of the loop
- multiple 'runs' at the same time, so loops overlap
- samples from soundcloud
- on mobile, show video full screen, and trigger
the z-index when the video is triggered on the pattern
- Songs as embeddable iframes
- Setting for video quality