// create a new branch for webpack
var gulp = require('gulp');
var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var streamqueue = require('streamqueue');
var shell = require('gulp-shell');
var htmlreplace = require('gulp-html-replace');
var uglify = require('gulp-uglify');
var webpack = require('webpack-stream');
var ngAnnotate = require('gulp-ng-annotate');

var srcFolderName = 'src';
var distFolderName = 'dist';

// Couldn't get require's optimizer to work
// from gulp, because the best package for the job, amdOptimizer,
// had problems with relative paths, and the creator said he has no more
// time to maintain it, and that they switched to webpack

// Packery and it's dependencies for some  reson
// have relative paths, so the baseUrl MUST be of their parent folder

gulp.task('js', shell.task([
    'r.js -o src/build.js',
    'r.js -o src/build-dev.js'
]));

// gulp.task('sass:watch', function() {
//     gulp.watch(srcFolderName + '/**/*.scss', ['sass-dev']);
// });

gulp.task('css-dev', function() {
    var sassStream = gulp.src(srcFolderName + '/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError));

    var cssStream = gulp.src('bower_components/angular-dragula/dist/dragula.css');

    // streqmueue merges the streams in the given order
    return streamqueue({
            objectMode: true
        }, sassStream, cssStream)
        .pipe(concat('style.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(srcFolderName));
});


gulp.task('css', function() {
    var sassStream = gulp.src(srcFolderName + '/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError));

    var cssStream = gulp.src('bower_components/angular-dragula/dist/dragula.min.css');

    return streamqueue({
            objectMode: true
        }, sassStream, cssStream)
        .pipe(concat('style.css'))
        .pipe(minifyCss())
        .pipe(gulp.dest(distFolderName));
});

gulp.task('html', function() {
    return gulp.src(srcFolderName + '/**/*.html')
        .pipe(htmlreplace({ 'js': 'script.js' }))
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(distFolderName));
});

gulp.task('library-html', function() {
    return gulp.src(srcFolderName + '/library/**/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(distFolderName + '/library/'));
});

gulp.task('webpack', function() {
    return gulp.src(srcFolderName + '/library/core/bootstrap.js')
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest(distFolderName + '/library/'));
});

gulp.task('webpack-dev', function() {
    return gulp.src(srcFolderName + '/library/core/bootstrap.js')
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(ngAnnotate())
        .pipe(gulp.dest(srcFolderName + '/library/'));
});

gulp.task('default', ['js', 'css-dev', 'css', 'html', 
    'webpack', 'webpack-dev', 'library-html']);