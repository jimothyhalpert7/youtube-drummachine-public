var express = require('express'),
    app = express(),
    port = process.env.PORT,
    MongoClient = require('mongodb').MongoClient;

var loopRedirectPath = '/#';
var url = *;

if (process.env['DEV'] === 'true') {
    console.log('DEV mode');
    url = 'mongodb://' + process.env.IP + ':27017/';
    // The dirname is needed by could9
    app.use(express.static('src'));
    app.use('/node_modules', express.static(__dirname + '/node_modules'));
    app.use('/bower_components', express.static(__dirname + '/bower_components'));    
} else {
    app.use(express.static('dist'));
}

// Need this to parse the req.body
// in the post method
app.configure(function() {
    app.use(express.bodyParser());
});

// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
    if (err)
        throw "Error connecting mongo:" + JSON.stringify(err);

    console.log("Connected to mnogoDB");

    var collection = db.collection('documents');

    app.post('/add', function(req, res) {
        console.log("Adding document: " + JSON.stringify(req.body));
        var song = req.body;
        song.views = 0;
        song.votes = 0;
        song.date = (new Date()).toISOString();

        collection.insert(song, function(err, result) {
            if (err) {
                console.log("Error /add: " + JSON.stringify(err));
                res.status(403).send(err);
            }
            else {
                console.log("Insert successful: " + JSON.stringify(result));
                res.status(200).send(result);
            }
        });
    });

    app.get('/getAll/:page/:size/:sort', function(req, res) {
        var page = Number(req.params.page) || 0;
        var size = Number(req.params.size) || 10;
        var sort = {};
        sort[req.params.sort] = -1; // sort by this param/descending

        collection.find({}).skip(page).limit(size).sort(sort).toArray(function(err, docs) {
            if (err) {
                console.log("Error /getAll: " + JSON.stringify(err));
                res.status(403).send(err);
            }
            else {
                console.log("Returning page: " + req.params.page +
                    ", size: " + req.params.size + ", length: " + docs.length);
                res.status(200).send(docs);
            }
        });
    });

    app.get('/count', function(req, res) {
        collection.find().count(function(err, count) {
            if (err) {
                console.log("Error /count: " + JSON.stringify(err));
                res.status(403).send(err);
            }
            else {
                console.log('count ', count);
                res.status(200).send({
                    count: count
                });
            }
        });
    });

    app.get('/loop/:id', function(req, res, next) {
        // console.dir(req);
        console.log('Request to /loop/' + req.params.id);
        // console.log('Encoded to: ' + encodeURIComponent(req.params.id));
        if (req.params.id) {
            collection.findOneAndUpdate({
                    _id: req.params.id
                }, {
                    $inc: {
                        views: 1
                    }
                }, {
                    projection: {
                        name: false,
                        votes: false,
                        views: false
                    }
                },
                function(err, r) {
                    if (err) {
                        console.log("Error /loop:id : " + JSON.stringify(err));
                        res.redirect('/');
                    }
                    else {
                        console.log("Found the following records");
                        console.log(r);
                        res.redirect(loopRedirectPath + encodeURIComponent(JSON.stringify(r.value)));
                    }
                });
        }
        else {
            res.redirect('/');
            // next();
        }
    });
});

app.listen(port);
console.log('Server started on port: ' + port);