/*global require*/

require([
    'angular'
], function (angular) { 
    require([
        'angularDragula',
        'ytdmService',
        'ytdmController'
    ], function(angularDragula, ytdmService, ytdmController) {
    // ], function() {
        window.onGapi = function() {
            window.onGoogleApis();
        };
        var app = angular.module('youTubeDrumMachine', [angularDragula(angular)]);
        // var app = angular.module('youTubeDrumMachine', []);
        
        app.run(function() {
            // This code loads the IFrame Player API code asynchronously.
            var tag = document.createElement('script');
            tag.src = "//www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            
            // This code loads the IFrame Player API code asynchronously.
            var tag = document.createElement('script');
            tag.src = "https://apis.google.com/js/client.js?onload=onGapi";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);            
        });
        
        app.controller('ytdmController', ytdmController);
        app.service('Service', ytdmService);     
        angular.bootstrap(document, ['youTubeDrumMachine']);
    })
});