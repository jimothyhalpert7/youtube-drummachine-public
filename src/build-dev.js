({
    baseUrl: '../bower_components',
    paths: {
        jquery: 'jquery/dist/jquery',
        angular: 'angular/angular',
        angularDragula: 'angular-dragula/dist/angular-dragula',
        jQueryBridget: 'jquery-bridget/jquery-bridget',
        requireLib: '../node_modules/requirejs/require',
        app:'../src/app',
        ytdmService: "../src/ytdmService",
        ytdmController: "../src/ytdmController"
    },
    shim: {
        angular: {
            exports: 'angular'
        },
        angularDragula: {
            deps: ['angular']
        },
        jQueryBridget: {
            deps: ['jquery']
        }
    },
    include: ["requireLib"],
    findNestedDependencies: true,
    optimize: "none",
    // uglify2: {
    //     //Example of a specialized config. If you are fine
    //     //with the default options, no need to specify
    //     //any of these properties.
    //     output: {
    //         beautify: false
    //     },
    //     compress: {
    //         sequences: true,
    //         global_defs: {
    //             DEBUG: false
    //         }
    //     },
    //     warnings: true,
    //     mangle: true
    // },    
    name: "app",
    out: "../src/script.js"
})