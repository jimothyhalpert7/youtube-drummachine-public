/*global require*/

require.config({
    baseUrl: '../bower_components',
    paths: {
        jquery: 'jquery/dist/jquery',
        angular: 'angular/angular',
        angularDragula: 'angular-dragula/dist/angular-dragula',
        app:'/app',
        ytdmService: "/ytdmService",
        ytdmController: "/ytdmController"
    },
    shim: {
        angular: {
            exports: 'angular'
        },
        angularDragula: {
            deps: ['angular']
        }
    },
    findNestedDependencies: true,
    optimize: "none",
    deps: ['app']
});