/*global require*/

define([
    'jquery',
    'packery/js/packery'//,
    // 'jQueryBridget'
    // ], function($, Packery, jQueryBridget) {
    ], function($, Packery) {
           // make Packery a jQuery plugin
    // jQueryBridget('packery', Packery, $ );
    
var ytdmController = function($scope, dragulaService, Service) {

    $scope.song = null;
    $scope.tracksLoaded = 0;
    $scope.showControls = null;
    $scope.randomSizes = false;
    $scope.players = []; //Service.players;
    var totalSteps = 0;
    var intervalID;
    var dragSourceId;

    function init(song) {
        $scope.song = song;
        $('#players').width(song.width);
        
        if ($scope.song.tracks.length > 0) {
            $scope.song.tracks.forEach(function(track) {
                addPlayer(track);
            });
            styleTrackLength($scope.song.trackLength);
        } else {
            $scope.showControls = 1;
            $('#loading').hide();
            $('#ytdmController').show();            
        }

        // initWatchers();

        $scope.savedUrl = $scope.song._id ? 
            location.origin + '/loop/' +  $scope.song._id : null;
        $scope.$digest();
    }

    Service.init(init);

    Array.prototype.move = function(old_index, new_index) {
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        return this; // for testing purposes
    };

    dragulaService.options($scope, 'bag-one', {
        moves: function(el, container, handle) {
            return handle.className === 'trackMoveHandle';
        }
    });

    var dragHandler = function(el, source) {
        console.log(el, source);
        dragSourceId = $(source).index();
    };

    var dropHandler = function(el, target, source, sibling) {
        var dragTargetId = $(target).index();
        $scope.players.move(dragSourceId, dragTargetId);

        var targetPlayer = $($('#players').children()[dragTargetId]);
        var sourcePlayer = $($('#players').children()[dragSourceId]);

        if (dragTargetId < dragSourceId) {
            targetPlayer.before(sourcePlayer);
        }
        else {
            targetPlayer.after(sourcePlayer);
        }

        new Packery('#players');
    };
    
    $scope.$on('bag-one.drop', dropHandler);
    $scope.$on('bag-one.drag', dragHandler);

    function playPad() {
        var currentStep = totalSteps % $scope.song.trackLength;
        console.timeEnd('> playPad, to ' + currentStep);
        console.time('> playPad, to ' + (currentStep + 1));
        // console.time('> playPad, step' + currentStep);
        $scope.currentStep = currentStep;
        // FIXME:
        // this seems to be needed only to change the css
        // classes of the current bar
        $scope.$digest();
        // console.log('currentStep: ', currentStep);
        
        if ($scope.currentStep == 0) {
            if($scope.song.muteOnStart){
                var playersLen = $scope.players.length;
                for (var i = 0; i < playersLen; i++) {
                  // NOTE: going with pause,
                  // cause with mute you have to later unmute
                  $scope.players[i].pauseVideo(); 
                    // if ($scope.song.tracks[index].track[0] !== true) {
                    //     player.setVolume(0);
                    // }
                }                
            }
        }

        var tracksLen = $scope.song.tracks.length;
        
        console.time('> playPad, play column');
        for (i = 0; i < tracksLen; i++) {
            // console.time('> playPad, track ' + i);
            var track = $scope.song.tracks[i];
            if (track.track[currentStep] == true) {
                var curPlayer = $scope.players[$scope.song.tracks.indexOf(track)];
                curPlayer.seekTo(track.start, true);
                curPlayer.playVideo();
                //setShortSamplePause();                
            }
            // console.timeEnd('> playPad, track' + i);
        }
        console.timeEnd('> playPad, play column');
        
        totalSteps++;
        // console.timeEnd('> playPad, step ' + currentStep);
    }

    // function intervalFn() {
    //     clearInterval(intervalID);
    //     playPad();
    //     intervalID = window.setInterval(intervalFn.bind(this), $scope.song.intervalLength);
    // }

    $scope.run = function(){
        if (intervalID) {
            clearInterval(intervalID);
        }

        // intervalID = window.setInterval(intervalFn.bind(this), $scope.song.intervalLength);
        console.time('> playPad, to ' + $scope.currentStep);
        intervalID = window.setInterval(playPad.bind(this), $scope.song.intervalLength);
    };

    $scope.pressPlayButton = function() {
        $scope.showControls = 0;
        $scope.run();
    };

    $scope.pause = function() {
        clearInterval(intervalID);

        var playersLen = $scope.players.length;
        for (var i = 0; i < playersLen; i++) {
            $scope.players[i].pauseVideo();
        }
    };

    $scope.deleteSample = function(index) {
        $scope.players[index].getIframe().remove();
        $scope.players.splice(index, 1);
        $scope.song.tracks.splice(index, 1);
        new Packery('#players');
    };

    $scope.changeSize = function(index) {
        $scope.players[index].setSize($scope.song.tracks[index].width, $scope.song.tracks[index].height);
        new Packery('#players');
    };

    $scope.changeTrackVolume = function(volume, index) {
        $scope.players[index].setVolume(volume);
    };
    
    var preloadPlayers = function() {
        // So that videos get pre-loaded,
        // and when triggered by seekTo, 
        // play right away
        var playersLen = $scope.players.length;
        for (var i = 0; i < playersLen; i++) {
            var playerObj = $scope.players[i];
            playerObj.playVideo();
            playerObj.seekTo($scope.song.tracks[i].start, true);
            playerObj.pauseVideo();
        }        
    };    
    
    function onPlayerReady(e) {
        new Packery('#players');
        var player = e.target;
        var playerId = player.getIframe().getAttribute("id").substr(6);
        var track = $scope.song.tracks[playerId];

        // Randomize track
        var trackIsEmpty = ! track.track.reduce(function(prevVal, curval) {
            return prevVal || curval;
        });
        
        if (trackIsEmpty && track.start == 0) {
            randomizeTrack(track, player, playerId);
        }
        
        player.setVolume(track.volume);
        player.setPlaybackQuality('medium'); 
        
        // Cause stupid bars don't get 
        // digested automatically
        $scope.tracksLoaded++;
        $scope.$digest();
        
        if ($('#loading').is(":visible") && playerId == ($scope.song.tracks.length - 1)) {
            $('#loading').hide();
            $('#ytdmController').show();
            preloadPlayers.call(this);            
            $scope.toggleFullScreen();
        }        
    }

    function randomizeTrack(track, player, playerId) {
        track.track = 
            track.track.map(function() { 
                return Math.random() < 0.5 ? true : false;
            });
        
        track.volume = Service.getRandomInt(20, 80);
        track.start = Service.getRandomInt(0, player.getDuration() - 1);
        
        if ($scope.randomSizes) {
            track.width = Service.getRandomInt(10, Math.floor($scope.song.width * 0.60));
            track.height = Service.getRandomInt(10, 360);
            $scope.changeSize(playerId);
        }        
    }
    
    function appendPlayerDiv(playerId) {
        // Generate player DOM's
        var playerDiv = $('<div/>', {
            id: playerId,
            // "class": "player masonry-brick"
        });
        $('#players').append(playerDiv);
    }

    function addPlayer(track) {
        var idNumber = $scope.song.tracks.indexOf(track);
        var playerId = 'player' + idNumber;
        appendPlayerDiv(playerId);
        var newPlayer = Service.createPlayer(playerId, track, onPlayerReady);
        $scope.players.push(newPlayer);
    }
    
    $scope.getIdFromUrl = function(url) {
        return url.substr(url.indexOf("v=") + 2, 11);
    };

    $scope.addTrack = function(newVideoId) {
        var lastTrack = $scope.song.tracks.slice(-1)[0] || {};
        var dimObj = lastTrack;
        
        switch ($scope.song.tracks.length) {
            case 0:
                dimObj = {
                    'width': 640,
                    'height': 360
                };
                break;
            case 1:
                dimObj = {
                    'width': 214,
                    'height': 120
                };
                break;
        }

        
        var newSample = Service.createSample(newVideoId, $scope.song.trackLength,
            dimObj.width, dimObj.height);
        $scope.song.tracks.push(newSample);
        if ($scope.showControls < 2) $scope.showControls = 2;
        addPlayer(newSample);
        
        // $scope.$apply();
    };
    
    $scope.changeSongWidth = function(width) {
        $('#players').width(width);
        // $('#players').packery();
        new Packery('#players');
    };
    
    function adjustTrackLength(newValue, oldValue) {
        var tracks = $scope.song.tracks;

        if (oldValue < newValue) {
            var trackExtension = new Array(newValue - oldValue).fill(false);
            tracks.forEach(function(track) {
                track.track = track.track.concat(trackExtension);
            });
        }
        else if (oldValue > newValue) {
            tracks.forEach(function(track) {
                track.track = track.track.slice(0, newValue);
            });
        }
        
        // $scope.$apply();
    }

    $scope.save = function(e) {
        $scope.savedUrl = "Saving (takes a sec)...";
        $scope.players.forEach(function(player, index) {
           $scope.song.tracks[index].volume = player.getVolume();
        });
        
        $scope.song.name = e;
        $scope.song._id = $scope.song.name.replace(/ /g, '_');

        var successHandler = function(res) {
            console.log(res);
            // need pathname for when in dev mode with /src/
            $scope.savedUrl = location.origin + '/loop/' + res.ops[0]._id;
            $scope.$apply();
        };
        
        var failHandler = function(res) {
            console.log(res);
            $scope.savedUrl = 'Name taken.';
            $scope.$apply();
        };
        
        Service.save($scope.song, successHandler, failHandler);
    };
    
    $scope.searchVideo = function(q) {
        gapi.client.youtube.search.list({
            q: q,
            part: 'snippet'
        }).execute(function(response) {
            $scope.searchResults = response.items.map(function(item) { 
                return {
                    title: item.snippet.title,
                    id: item.id.videoId
                };
            });
        });        
    };
    
    function changeSizeAll(newWidth) {
        var playersContainer = $('#players');
        var finalWidth = newWidth;
        
        if (!$scope.playersRatio) {
            $scope.playersRatio = playersContainer.width() / playersContainer.height();             
        }
        var deltaRatio = newWidth / playersContainer.width();

        $scope.song.tracks.forEach(function(track, index){
           var playerDiv = $('#player' + index);
           var newWidth =  playerDiv.width() * deltaRatio;
           var newHeight = playerDiv.height() * deltaRatio;
           $scope.players[index].setSize(newWidth, newHeight);
        });

        playersContainer.width(finalWidth);
    }
    
    $scope.toggleFullScreen = function() {
        var newWidth = $scope.song.width;            
 
        if (!$scope.fullScreen) {
            newWidth = $(window).width() - 256;
            var playersContainer = $('#players');
            var deltaRatio = newWidth / playersContainer.width();
            // Case when expanded height turns out more than the screen's
            if (playersContainer.height() * deltaRatio > $(window).height() - 64) {
                deltaRatio = ($(window).height() - 64) / playersContainer.height();
                newWidth = playersContainer.width() * deltaRatio; 
            }            
        }
        changeSizeAll(newWidth);          
        new Packery('#players');
        $scope.fullScreen = !$scope.fullScreen;
    };
    
    function styleTrackLength(newValue, oldValue) {
        var oldValue = oldValue || null;
        if (newValue > 16) {
            $('#trackControls').css({
                "align-items": "flex-start"
            });
        } else if (oldValue > 16) {
            $('#trackControls').css({
                "align-items": "center"
            });
        }
    }
    
    $scope.initTrackControls = function() {
        styleTrackLength($scope.song.trackLength);
    };
    
    $scope.toggleControls = function() {
        $scope.showControls = ($scope.showControls + 1) % 5;
    };
    
    $scope.changeTrackLength = function(newValue, oldValue) {
            adjustTrackLength(newValue, oldValue);
            styleTrackLength(newValue, oldValue);
    };

    $scope.setIsHorizontal = function(isHorizontal) {
        new Packery('#players', {
            isHorizontal: isHorizontal
        });
    };    
    
    function initWatchers() {
        $scope.$watch('song', function(newValue, oldValue) {
            if (newValue.isHorizontal !== oldValue.isHorizontal) {
                new Packery('#players', {
                    isHorizontal: newValue.isHorizontal
                });
            }

            if (newValue.trackLength !== oldValue.trackLength) {
                adjustTrackLength(newValue.trackLength, oldValue.trackLength);
                // checkControlsWidth();
            }
            
            if (newValue.trackLength > 16) {
                $('#trackControls').css({
                    "align-items": "flex-start"
                });
            } else if (oldValue.trackLength > 16) {
                $('#trackControls').css({
                    "align-items": "center"
                });
            }
            
            parent.location.hash = encodeURIComponent(JSON.stringify(newValue));
        }, true);
    }
};

ytdmController.$inject = ['$scope', 'dragulaService', 'Service'];

   return ytdmController; 
});