/*global require*/

define([
    'jquery'
], function($) {
    var ytdmService =  function($window, $rootScope) {
    
        var song = this.song = 'test';
        this.createSample = createSample;
        this.save = save;
        this.getRandomInt = getRandomInt;        

        $rootScope.$on('youtube.player.ended', function($event, player) {
            // play it again
            player.playVideo();
        });
    
        function createPlayer(playerId, sample, onPlayerReady) {
    
            // dragula([document.querySelector('.bar')]);
    
            var player = new YT.Player(playerId, {
                width: sample.width,
                height: sample.height,
                videoId: sample.videoId,
                playerVars: {
                    'controls': 2,
                    'showinfo': 0,
                    'modestbranding': 1
                },
                events: {
                    'onReady': onPlayerReady
                }
            });
    
            return player;
        }
    
        this.createPlayer = createPlayer;
    
        function createSample(videoId, trackLength, inWidth, inHeight) {
            var track = new Array(trackLength).fill(false);
            var width = inWidth ? inWidth : Math.floor(($(window).width() - 100) / 2);
            var height = inHeight ? inHeight : 336;
    
            var sample = sample ? sample : {
                videoId: videoId,
                height: height,
                width: width,
                start: 0,
                end: null,
                volume: 0,
                track: track
            };
    
            return sample;
        }
    
        function save(song, successHandler, failHandler) {
            var settings = {
                url: '/add',
                method: "POST",
                data: JSON.stringify(song),
                contentType: "application/json",
                processData: false,
                context: this
            };
    
            $.ajax(settings).done(successHandler).fail(failHandler);            
        }
        
        /**
         * Returns a random integer between min (inclusive) and max (inclusive)
         * Using Math.round() will give you a non-uniform distribution!
         */
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        
        // song - json object from url's hash parameter
        function init(handler) {
            var song = {
                intervalLength: this.getRandomInt(150, 450),
                trackLength: 8,
                muteOnStart: false,
                isHorizontal: true,
                width: 854,
                tracks: []
            };
    
            if (parent.location.hash.substr(1) != "") {
                song = JSON.parse(decodeURIComponent(parent.location.hash.substr(1)));
            }
    
            $window.onYouTubeIframeAPIReady = function() {
                if (handler) handler(song);
            };

            $window.onGoogleApis = function() {
                gapi.client.setApiKey('AIzaSyAdZ-1LnFtDZITMiH1kWFSYhA7NDcvXjiM');
                gapi.client.load('youtube', 'v3', function() {
                        console.log('gapi loaded');
                });                
            }; 
        }
    
        this.init = init;
    };
    
    ytdmService.$inject = ['$window', '$rootScope'];    
    
    return ytdmService;
});
