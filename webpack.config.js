'use strict';  
var webpack = require('webpack'),  
path = require('path');

var APP = __dirname + '/src/library';

module.exports = {  
    context: APP,
     entry:  './core/bootstrap.js',
    output: {
        path: APP,
        filename: 'bundle.js'
    },
  module: {
    loaders: [
      { test: /\.css$/, loader: "style-loader!css-loader" }
    ]
  }   
};